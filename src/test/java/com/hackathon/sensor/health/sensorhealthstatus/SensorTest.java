package com.hackathon.sensor.health.sensorhealthstatus;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hackathon.sensor.health.common.CommonConstants.SensorStatus;
import com.hackathon.sensor.health.sensor.Sensor;
import com.hackathon.sensor.health.sensor.SensorRequestModel;
import com.hackathon.sensor.health.sensor.SensorService;
import com.hackathon.sensor.health.sensor.location.Location;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SensorTest {

	@Autowired SensorService sensorService;
	
	@Test
	public void insertSensor(){
		Location location = new Location();
		location.setId(1l);
		location.setName("SDB01");
		Sensor sensor = new Sensor();
		sensor.setId(1L);
		sensor.setLocation(location);
		sensor.setName("Sensor001");
		sensor.setSensorStatus(SensorStatus.DOWN);
		sensorService.insertSensors(Arrays.asList(sensor));
	}
	
	@Test
	public void performActionOnSensor(){
		SensorRequestModel model = new SensorRequestModel();
		Location location = new Location();
		location.setId(1l);
		location.setName("SDB01");
		model.setId(1L);
		model.setLocation(location);
		model.setName("Sensor001");
		model.setSensorStatus(SensorStatus.UP);
		sensorService.performActionOnSensor(model);
	}
	
}
