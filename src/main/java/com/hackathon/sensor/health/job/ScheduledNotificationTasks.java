package com.hackathon.sensor.health.job;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hackathon.sensor.health.notification.SensorHealthReportNotifier;

@Component
public class ScheduledNotificationTasks {

    private static final Logger log = LoggerFactory.getLogger(ScheduledNotificationTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired SensorHealthReportNotifier sensorHealthReportNotifier;
    
    @Scheduled(fixedRate = 5000)
    public void notifyDownSensors() {
        log.info("The time is now {}", dateFormat.format(new Date()));
        sensorHealthReportNotifier.startNotifing();
    }
}