package com.hackathon.sensor.health.common;

public class CommonConstants {

	public enum State{
		ACTIVE,
		INACTIVE
	}
	public enum SensorStatus{
		DOWN,
		DOWN_PARTIAL,
		WARNING,
		UNUSUAL,
		UP,
		PAUSED,
		UNKNOWN
	}
}
