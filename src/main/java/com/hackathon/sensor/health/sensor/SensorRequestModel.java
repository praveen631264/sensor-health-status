package com.hackathon.sensor.health.sensor;

import com.hackathon.sensor.health.common.CommonConstants.SensorStatus;
import com.hackathon.sensor.health.common.CommonFields;
import com.hackathon.sensor.health.sensor.location.Location;

public class SensorRequestModel extends CommonFields{

	private Long id;
	private String name;
	private SensorStatus sensorStatus;
	
	private Location location;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SensorStatus getSensorStatus() {
		return sensorStatus;
	}

	public void setSensorStatus(SensorStatus sensorStatus) {
		this.sensorStatus = sensorStatus;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	
	
}
