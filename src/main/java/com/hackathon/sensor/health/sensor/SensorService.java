package com.hackathon.sensor.health.sensor;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackathon.sensor.health.sensor.history.SensoryHistory;
import com.hackathon.sensor.health.sensor.history.SensoryHistoryRepository;

@Service
public class SensorService {

	@Autowired
	SensorRepository sensorRepository;
	
	@Autowired
	SensoryHistoryRepository sensoryHistoryRepository;
	
	public List<Sensor> listSensors(){
		return sensorRepository.findAll();
	}
	
	public Sensor findSensorById(Long id){
		Optional<Sensor> sensor = sensorRepository.findById(id);
		if(sensor.isPresent())
			return sensor.get();
		return null;
	}
	
	public List<Sensor> insertSensors(List<Sensor> sensors){
		return sensorRepository.saveAll(sensors);
	}
	
	public SensorRequestModel performActionOnSensor(SensorRequestModel sensor){
		SensoryHistory history = new SensoryHistory();
		Date currentDate = new Date();
		BeanUtils.copyProperties(sensor, history);
		history.setCreatedBy("USER");
		history.setCreatedDate(currentDate);
		history.setLastModifiedBy("USER");
		history.setLastModifiedDate(currentDate);
		sensoryHistoryRepository.save(history);
		sensorRepository.deleteById(sensor.getId());
		return sensor;
	}
}
