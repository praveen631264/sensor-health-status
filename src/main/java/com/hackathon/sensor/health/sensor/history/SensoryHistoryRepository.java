package com.hackathon.sensor.health.sensor.history;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SensoryHistoryRepository extends MongoRepository<SensoryHistory, String>{

}
