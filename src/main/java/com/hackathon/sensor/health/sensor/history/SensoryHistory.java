package com.hackathon.sensor.health.sensor.history;

import org.springframework.data.mongodb.core.mapping.Document;

import com.hackathon.sensor.health.common.CommonConstants.SensorStatus;
import com.hackathon.sensor.health.common.CommonConstants.State;
import com.hackathon.sensor.health.common.CommonFields;
import com.hackathon.sensor.health.sensor.location.Location;

@Document("sensor_history")
public class SensoryHistory extends CommonFields {

	private Long id;
	private String name;
	private SensorStatus sensorStatus;
	
	private Location location;
	
	private State state;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SensorStatus getSensorStatus() {
		return sensorStatus;
	}

	public void setSensorStatus(SensorStatus sensorStatus) {
		this.sensorStatus = sensorStatus;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "SensoryHistory [id=" + id + ", name=" + name
				+ ", sensorStatus=" + sensorStatus + ", location=" + location
				+ ", state=" + state + ", getId()=" + getId() + ", getName()="
				+ getName() + ", getSensorStatus()=" + getSensorStatus()
				+ ", getLocation()=" + getLocation() + ", getState()="
				+ getState() + ", getCreatedBy()=" + getCreatedBy()
				+ ", getCreatedDate()=" + getCreatedDate()
				+ ", getLastModifiedBy()=" + getLastModifiedBy()
				+ ", getLastModifiedDate()=" + getLastModifiedDate()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

}
