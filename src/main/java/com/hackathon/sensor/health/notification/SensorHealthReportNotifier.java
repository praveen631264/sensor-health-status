package com.hackathon.sensor.health.notification;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hackathon.sensor.health.mail.EmailService;
import com.hackathon.sensor.health.mail.Mail;
import com.hackathon.sensor.health.sensor.Sensor;
import com.hackathon.sensor.health.sensor.SensorService;

import freemarker.template.TemplateException;

@Component
public class SensorHealthReportNotifier {

	@Autowired SensorService sensorService;
	@Autowired EmailService emailService;
	
	public void startNotifing(){
		List<Sensor> sensors = sensorService.listSensors();
		if(sensors!=null && sensors.size() > 0){
			notifyByEmail(sensors);	
		}
	}

	private void notifyByEmail(List<Sensor> sensors) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("sensorData", sensors);
		Mail mail = new Mail();
		mail.setFrom("praveen72127@gmail.com");
		mail.setModel(model);
		mail.setSubject("Sensor Status Report");
		mail.setTemplateName("sensor-status-report.ftl");
		mail.setTo("praveen631264@gmail.com");
		try {
			emailService.send(mail);
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}

	
	
}
