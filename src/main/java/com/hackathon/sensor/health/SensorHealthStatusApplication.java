package com.hackathon.sensor.health;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.hackathon.sensor.health.job.ScheduledNotificationTasks;

@SpringBootApplication
@EnableScheduling
public class SensorHealthStatusApplication {

	private static final Logger log = LoggerFactory.getLogger(ScheduledNotificationTasks.class);
	
	public static void main(String[] args) {
		ConfigurableApplicationContext applicationContext = SpringApplication.run(SensorHealthStatusApplication.class, args);
		String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
		log.info("****************Bean Initailized List*************************\n");
		for(String beanName : beanDefinitionNames){
			log.info(beanName+"\n");
		}
		log.info("**************************************************************\n");
	}
}
