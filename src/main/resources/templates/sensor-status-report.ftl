<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Action required for the below sensor report</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
<body>
	<table align="center">
	<th>Sensor ID</th>
	<th>Sensor Name</th>
	<th>Sensor Location</th>
	<th>Sensor Status</th>
	<#list sensorData as item>
	    <tr>
	    <td>${item.id}</td>
	    <td>${item.name}</td>
	    <td>${item.location.name}</td>
	    <td>${item.sensorStatus}</td>
	    </tr>
	</#list>
	</table>
</body>